defmodule Physics.Utils.Calculate do

  def squared(value) when is_number(value), do: value * value

  def cubed(value) when is_number(value), do: value * value * value

  def square_root(value) when is_number(value), do: :math.sqrt(value)

end
