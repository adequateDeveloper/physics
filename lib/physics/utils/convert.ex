defmodule Physics.Utils.Convert do

  @miles_conversion_factor  5.36819e-6
  @meters_conversion_factor 3.335638620368e-9
  @feet_conversion_factor   1.016702651488166404e-9
  @inches_conversion_factor 8.472522095734715723e-11

  def to_nearest_tenth(value) when is_number(value), do: round_to(value, 1)

  def to_km(value) when is_number(value), do: value / 1000

  def to_meters(value) when is_number(value), do: value * 1000

  def seconds_to_hours(value) when is_number(value), do: (value / 3600) |> to_nearest_tenth

  def to_light_seconds(arg) when is_tuple(arg), do: to_light_seconds(arg, precision: 3)
  def to_light_seconds({unit, value}, precision: precision)
    when is_atom(unit) and is_number(value) and is_integer(precision) do
    case unit do
      :miles  -> from_miles(value)
      :meters -> from_meters(value)
      :feet   -> from_feet(value)
      :inches -> from_inches(value)
    end |> round_to(precision)
  end

  defp from_miles(value),  do: value * @miles_conversion_factor
  defp from_meters(value), do: value * @meters_conversion_factor
  defp from_feet(value),   do: value * @feet_conversion_factor
  defp from_inches(value), do: value * @inches_conversion_factor

  defp round_to(value, precision), do: Float.round(value, precision)

end
