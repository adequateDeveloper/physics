defmodule Physics.Utils.Solar do

  # filter for flares with power > 1000
  def no_eva(flares) when is_list(flares) do
    Enum.filter flares, &( power(&1) > 1000 )
  end

  # get the largest power calcuation
  def deadliest(flares) when is_list(flares) do
    Enum.map(flares, &( power(&1) ))
      |> Enum.max
  end

  def flare_list(flares) when is_list(flares) do
    for flare <- flares, do: %{power: power(flare), is_deadly: power(flare) > 1000}
  end

#------------------------------------------------------------------------------
# These next set of functions are equivilent but use differing approaches to
# to summing values in a list.
# 1. Uses list comprehension with Enum.sum
# 2. Next 2 are identical - uses Enum.reduce
# 3. Last 5 use classic recursion with entry/exit point function heads
#------------------------------------------------------------------------------

  # the equivilent of the next method below but using comprehension
  # def total_flare_power(flares) do
  #   (for flare <- flares, do: power(flare))
  #     |> Enum.sum
  # end

  # the equivilent of next method below
  def total_flare_power(flares) do
    Enum.reduce flares, 0, &( power(&1) + &2 )
  end

  # Enum.reduce equivilent of the recursive approach below
  # def total_flare_power(flares) do
  #   Enum.reduce flares, 0, fn(flare, total) ->
  #     power(flare) + total
  #   end
  # end

#----------------------------------------------------------

  # recursive entry point
  # def total_flare_power(flares) when is_list(flares) do
  #   total_flare_power(flares, 0)
  # end

  # recursive exit point
  # defp total_flare_power([], total), do: total

  # recursive accumulators with tolerance factors
  # defp total_flare_power([%{classification: :C, scale: s} | tail], total) do
  #   new_total = s * 0.78 + total
  #   total_flare_power(tail, new_total)
  # end

  # defp total_flare_power([%{classification: :M, scale: s} | tail], total) do
  #   new_total = s * 10 * 0.92 + total
  #   total_flare_power(tail, new_total)
  # end

  # defp total_flare_power([%{classification: :X, scale: s} | tail], total) do
  #   new_total = s * 1000 * 0.68 + total
  #   total_flare_power(tail, new_total)
  # end

  defp power(%{classification: :C, scale: s}), do: s * 1
  defp power(%{classification: :M, scale: s}), do: s * 10
  defp power(%{classification: :X, scale: s}), do: s * 1000

end
