
# Escape velocity (v sub e) = the square root of: 2 * Newton’s Gravitational Constant (G)
# of whatever planet, the mass (M) of that body, all divided by (R), which is
# the radius of that planet.

defmodule Physics.Rocketry do

  alias Physics.Utils.{Laws, Convert, Calculate}

  def orbital_acceleration(planet, height) when is_map(planet) and is_number(height) do
    (orbital_speed(planet, height)
      |> Calculate.squared) / orbital_radius(planet, height)
  end

  def orbital_speed(planet, height) when is_map(planet) and is_number(height) do
    Laws.newtons_gravitational_constant * planet.mass / orbital_radius(planet, height)
      |> Calculate.square_root
  end

  def orbital_term(planet, height) when is_map(planet) and is_number(height) do
    4 * (:math.pi |> Calculate.squared) * (orbital_radius(planet, height)
      |> Calculate.cubed) / (Laws.newtons_gravitational_constant * planet.mass)
      |> Calculate.square_root
      |> Convert.seconds_to_hours
  end

  def orbital_radius(planet, height) when is_map(planet) and is_number(height) do
    planet.radius + (height |> Convert.to_meters)
  end

end
