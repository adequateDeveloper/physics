# Physics

[![pipeline status](https://gitlab.com/adequateDeveloper/physics/badges/master/pipeline.svg)](https://gitlab.com/adequateDeveloper/physics/commits/master)

Following the Red:4 Elixir tutorial

04/10/2019 Notes to self:

This is an old learning Elixir project that looks to be a good candidate for re-purposing as a learning platform for basic pattern-based testing techniques.

- Rebuild the project from scratch with latest Elixir.

- Keep the overall structure and tests.

- Add separate pattern-based tests to compare against the existing unit tests.