defmodule Physics.PlanetTest do
  use ExUnit.Case, async: true

  alias Physics.Planet
  @mut TestHelper.module_under_test __MODULE__
  @err_reason "no function clause matching in"


  setup do
    {:ok, planets: Planet.select}
  end

  test "planets are correct", %{planets: planets} do
    assert planets[:mars]    == %Planet{name: "Mars",    type: :rocky,   mass: 6.41e23,  radius: 3.37e6,  ev: 5.0}
    assert planets[:moon]    == %Planet{name: "Moon",    type: :rocky,   mass: 7.35e22,  radius: 1.738e6, ev: 2.4}
    assert planets[:venus]   == %Planet{name: "Venus",   type: :rocky,   mass: 4.86e24,  radius: 6.05e6,  ev: 10.4}
    assert planets[:earth]   == %Planet{name: "Earth",   type: :rocky,   mass: 5.972e24, radius: 6.37e6,  ev: 11.2}
    assert planets[:saturn]  == %Planet{name: "Saturn",  type: :gaseous, mass: 5.68e26,  radius: 6.02e7,  ev: 35.5}
    assert planets[:uranus]  == %Planet{name: "Uranus",  type: :gaseous, mass: 8.68e25,  radius: 2.55e7,  ev: 21.3}
    assert planets[:mercury] == %Planet{name: "Mercury", type: :rocky,   mass: 3.3e23,   radius: 2.439e6, ev: 4.2}
    assert planets[:jupiter] == %Planet{name: "Jupiter", type: :gaseous, mass: 1.89e27,  radius: 7.14e7,  ev: 59.4}
    assert planets[:neptune] == %Planet{name: "Neptune", type: :gaseous, mass: 1.02e26,  radius: 2.47e7,  ev: 23.5}
  end

  test "Accounting for Venus blowing up", %{planets: planets} do
    # venus = Planet.select[:venus]
    venus = planets[:venus]
    venus = %{venus | ev: 7.2}
    assert venus.ev == 7.2
  end

  test "escape velocity of planet X is correct" do
    ev = %{mass: 4.0e22, radius: 6.21e6}
      |> Planet.escape_velocity
    assert ev == 0.9
  end

  test "a non-map planet fails calcuation" do
    functionClauseErrorMessage = "#{@err_reason} #{@mut}.escape_velocity/1"
    assert_raise FunctionClauseError, functionClauseErrorMessage, fn ->
      [mass: 4.0e22, radius: 6.21e6] |> Planet.escape_velocity
    end
  end

end
