defmodule Physics.RocketryTest do
  use ExUnit.Case, async: true

  alias Physics.{Planet, Rocketry}
  alias Physics.Utils.Convert

  @mut TestHelper.module_under_test __MODULE__
  @err_reason "no function clause matching in"


  doctest Rocketry

  test "orbital speed calculation of Earth at 100km is correct" do
    os = Rocketry.orbital_speed(Planet.select[:earth], 100)
    assert Convert.to_nearest_tenth(os) == 7846.4
  end

  test "orbital acceleration for Earth at 100km is correct" do
    oa = Rocketry.orbital_acceleration(Planet.select[:earth], 100)
    assert Convert.to_nearest_tenth(oa) == 9.5
  end

  test "Orbital term for 100km above earth is correct" do
    term = Physics.Rocketry.orbital_term(Planet.select[:earth], 100)
    assert 1.4 <= term and term <= 1.5
  end

  test "orbital speed using non-number fails calculation" do
    functionClauseErrorMessage = "#{@err_reason} #{@mut}.orbital_speed/2"
    assert_raise FunctionClauseError, functionClauseErrorMessage, fn ->
      Rocketry.orbital_speed(Planet.select[:earth], "100")
    end
  end

  test "orbital acceleration using non-number fails calculation" do
    functionClauseErrorMessage = "#{@err_reason} #{@mut}.orbital_acceleration/2"
    assert_raise FunctionClauseError, functionClauseErrorMessage, fn ->
      Rocketry.orbital_acceleration(Planet.select[:earth], "100")
    end
  end

  test "orbital term using non-number fails calculation" do
    functionClauseErrorMessage = "#{@err_reason} #{@mut}.orbital_term/2"
    assert_raise FunctionClauseError, functionClauseErrorMessage, fn ->
      Rocketry.orbital_term(Planet.select[:earth], "100")
    end
  end

end
