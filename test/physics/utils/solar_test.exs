defmodule Physics.Utils.SolarTest do
  use ExUnit.Case, async: true

  alias Physics.Utils.Solar

  @mut TestHelper.module_under_test __MODULE__
  @err_reason "no function clause matching in"

  # one-time set up for all tests
  setup_all do
    # classification multiplier: X = 1000, M = 10, C = 1
    flares = [
      %{classification: :X, scale: 99,   date: ~D[1859-08-29]},
      %{classification: :M, scale: 5.8,  date: ~D[2015-01-12]},
      %{classification: :M, scale: 1.2,  date: ~D[2015-02-09]},
      %{classification: :C, scale: 3.2,  date: ~D[2015-04-18]},
      %{classification: :M, scale: 83.6, date: ~D[2015-06-23]},
      %{classification: :C, scale: 2.5,  date: ~D[2015-07-04]},
      %{classification: :X, scale: 72,   date: ~D[2012-07-23]},
      %{classification: :X, scale: 45,   date: ~D[2003-11-04]}
    ]
    {:ok, data: flares}
  end

  test "We have 8 solar flares", %{data: flares} do
    assert length(flares) == 8
  end

  test "no_eva/1 returns only :X classifications", %{data: flares} do
    results = Solar.no_eva flares
    assert Enum.all? results, &( &1.classification == :X )
  end

  test "deadliest/1 calculates correctly", %{data: flares} do
    assert Solar.deadliest(flares) == 99000
  end

  test "no_eva/1 requires list" do
    functionClauseErrorMessage = "#{@err_reason} #{@mut}.no_eva/1"
    assert_raise FunctionClauseError, functionClauseErrorMessage, fn ->
      Solar.no_eva {}
    end
  end

  test "deadliest/1 requires list" do
    functionClauseErrorMessage = "#{@err_reason} #{@mut}.deadliest/1"
    assert_raise FunctionClauseError, functionClauseErrorMessage, fn ->
      Solar.deadliest {}
    end
  end

  test "total_flare_power calculates correctly", %{data: flares} do
    total = flares |> Solar.total_flare_power
    # assert total == 147_717.966   # total when tolerance factor enabled
    assert total == 216_911.7
  end

  test "flare_list produces 3 deadly", %{data: flares} do
    all = flares |> Solar.flare_list
    # all |> IO.inspect
    assert 3 == length Enum.filter(all, &( &1.is_deadly ))
  end

end
