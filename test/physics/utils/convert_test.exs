defmodule Physics.Utils.ConvertTest do
  use ExUnit.Case, async: true

  alias Physics.Utils.Convert
  @mut TestHelper.module_under_test __MODULE__
  @err_reason "no function clause matching in"


  test "to_nearest_tenth is correct" do
    assert 0.109 |> Convert.to_nearest_tenth == 0.1
  end

  test "to_km is correct" do
    assert 1000 |> Convert.to_km == 1.0
  end

  test "to_meters is correct" do
    assert 1 |> Convert.to_meters == 1000
  end

  test "seconds_to_hours is correct" do
    assert 3600 |> Convert.seconds_to_hours == 1.0
    assert 1800 |> Convert.seconds_to_hours == 0.5
  end

  test "miles to_light_seconds is correct" do
    assert {:miles, 1_000_000} |> Convert.to_light_seconds(precision: 1) == 5.4
  end

  test "miles to_light_seconds is correct to a default precision" do
    assert {:miles, 1_000_000} |> Convert.to_light_seconds == 5.368
  end

  test "meters to_light_seconds is correct" do
    assert {:meters, 1_000_000_000} |> Convert.to_light_seconds(precision: 1) == 3.3
  end

  test "meters to_light_seconds is correct to a default precision" do
    assert {:meters, 1_000_000_000} |> Convert.to_light_seconds == 3.336
  end

  test "feet to_light_seconds is correct" do
    assert {:feet, 1_000_000_000} |> Convert.to_light_seconds(precision: 1) == 1.0
  end

  test "feet to_light_seconds is correct to a default precision" do
    assert {:feet, 1_000_000_000} |> Convert.to_light_seconds == 1.017
  end

  test "inches to_light_seconds is correct" do
    assert {:inches, 1_000_000_000_000} |> Convert.to_light_seconds(precision: 1) == 84.7
  end

  test "inches to_light_seconds is correct to a default precision" do
    assert {:inches, 1_000_000_000_000} |> Convert.to_light_seconds == 84.725
  end

  test "a non-tuple fails calcuation" do
    functionClauseErrorMessage = "#{@err_reason} #{@mut}.to_light_seconds/1"
    assert_raise FunctionClauseError, functionClauseErrorMessage, fn ->
      [:inches, 1_000_000_000_000] |> Convert.to_light_seconds
    end
  end

  test "a non-atom key fails calcuation" do
    functionClauseErrorMessage = "#{@err_reason} #{@mut}.to_light_seconds/2"
    assert_raise FunctionClauseError, functionClauseErrorMessage, fn ->
      {"inches", 1_000_000_000_000} |> Convert.to_light_seconds
    end
  end

  test "a non-number value fails calcuation" do
    functionClauseErrorMessage = "#{@err_reason} #{@mut}.to_light_seconds/2"
    assert_raise FunctionClauseError, functionClauseErrorMessage, fn ->
      {:inches, "1_000_000_000_000"} |> Convert.to_light_seconds
    end
  end

  test "a non-integer precision fails calcuation" do
    functionClauseErrorMessage = "#{@err_reason} #{@mut}.to_light_seconds/2"
    assert_raise FunctionClauseError, functionClauseErrorMessage, fn ->
      {:inches, 1_000_000_000_000} |> Convert.to_light_seconds(precision: 2.5)
    end
  end

end
