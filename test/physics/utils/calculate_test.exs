defmodule Physics.Utils.CalculateTest do
  use ExUnit.Case, async: true

  alias Physics.Utils.Calculate
  @mut TestHelper.module_under_test __MODULE__
  @err_reason "no function clause matching in"


  test "square_root is correct" do
    assert Calculate.square_root(9.0) == 3.0
  end

  test "squared is correct" do
    assert Calculate.squared(4.0) == 16.0
  end

  test "cubed is correct" do
    assert Calculate.cubed(3.0) == 27.0
  end

  test "a non-number fails calcuation" do
    functionClauseErrorMessage = "#{@err_reason} #{@mut}.cubed/1"
    assert_raise FunctionClauseError, functionClauseErrorMessage, fn ->
      Calculate.cubed("3.0")
    end
  end

end
