defmodule Physics.Utils.LawsTest do
  use ExUnit.Case, async: true

  alias Physics.Utils.Laws

  doctest Laws

  test "newtons_gravitational_constant is correct" do
    assert Laws.newtons_gravitational_constant == 6.67e-11
  end

end
