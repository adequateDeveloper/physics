
ExUnit.start()

defmodule TestHelper do

  def module_under_test(module) when is_atom(module) do
    as_string = module |> Atom.to_string                    # ex: "Elixir.Physics.Utils.SolarTest"
    elements = String.split(as_string, ["Elixir.","Test"])  # ex: ["", "Physics.Utils.Solar", ""]
    Enum.at(elements, -2)
  end

end
